

do {
    let userName = prompt('What is your name?');
    let userAge = +prompt(`How old are you?`);

    if (userAge < 18) {
        alert('You are not allowed to visit this website');
    } else if (userAge >= 18 && userAge <= 22) {
        const userChoice = confirm('Are you sure you want to continue?');
        if (userChoice) {
            alert(`Welcome, ${userName}`);
        } else {
            alert('You are not allowed to visit this website');
        }
    } else {
        alert(`Welcome, ${userName}`);
    }

    if (!(userName === '') && !(isNaN(userAge)))
    {
        break;
    }
} while (true);