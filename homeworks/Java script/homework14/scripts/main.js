
$('.header-navbar-box__list-elem__items a, .top').mPageScroll2id();

const btnTop = document.querySelector('.top');

btnTop.onclick = function() {
    window.scrollTo(pageXOffset, 0);
};

window.addEventListener('scroll', function() {
    btnTop.hidden = (pageYOffset < document.documentElement.clientHeight);
});

$("#hide_btn").click(function () {
    $(".grid-wrap-top-rated").slideToggle("slow");
});