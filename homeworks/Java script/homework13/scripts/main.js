
const btn = document.querySelector('.new_btn');

let bodyColor = document.getElementsByTagName('body')[0];

if (localStorage.getItem("bgColor") !== null) {
    const color = localStorage.getItem("bgColor");
    bodyColor.style.backgroundColor = color;
}

btn.addEventListener('click', function () {

    if (bodyColor.style.backgroundColor !== "pink") {
        bodyColor.style.backgroundColor = "pink";
        localStorage.setItem("bgColor", "pink");
    } else {
        bodyColor.style.backgroundColor = "darkslateblue";
        localStorage.setItem("bgColor", "darkslateblue");
    }

});