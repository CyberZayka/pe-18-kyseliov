
function makeListElementsFromArray(userGetArray) {

    let listOfElements = userGetArray.map(function (itemOfArray) {

        let ul = document.createElement('ul');
        let li = document.createElement('li');
        li.innerHTML = itemOfArray;
        ul.appendChild(li);
        document.body.appendChild(ul);

    });

}

let userTakeArray = ['Kyiv','Lviv','Odessa','Kharkiv','Dnipro'];

makeListElementsFromArray(userTakeArray);


