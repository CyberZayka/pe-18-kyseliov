
function filterBy(userArray, userTypeOfData) {

if (userTypeOfData === 'string') {
    for (let i = 0; i < userArray.length; i++) {

        if (typeof userArray[i] === 'string')
        {
            let parseData = userArray.filter(function (userString) {
                return typeof userString !== 'string';
            });
            console.log(parseData);
        }

    }
}
else if (userTypeOfData === 'number') {
    for (let i = 0; i < userArray.length; i++)
    {
        if (typeof userArray[i] === 'number')
        {
            let parseData = userArray.filter(function (userNumber) {
                return typeof userNumber !== 'number';
            });
            console.log(parseData);
        }
    }
}
else if (userTypeOfData === 'false' || userTypeOfData === 'true')
{
    for (let i = 0; i < userArray.length; i++)
    {
        if (typeof userArray[i] === "boolean")
        {
            let parseData = userArray.filter(function (userTrueFalse) {
                return typeof userTrueFalse !== "boolean";
            });
            console.log(parseData);
        }
    }
}
else if (userTypeOfData === 'null')
{
    for (let i = 0; i < userArray.length; i++)
    {
        if (typeof userArray[i] === "object")
        {
            let parseData = userArray.filter(function (userNull) {
                return typeof userNull !== "object";
            });
            console.log(parseData);
        }
    }
}
else if (userTypeOfData === 'undefined')
{
    for (let i = 0; i < userArray.length; i++)
    {
        if (typeof userArray[i] === "undefined")
        {
            let parseData = userArray.filter(function (userUndefined) {
                return typeof userUndefined !== "undefined";
            });
            console.log(parseData);
        }
    }
}
else {
    console.log('Unknown type of data you have wrote. Try again please');
}

}

let changeArrayOne = ['hello',23,null, true,false,undefined];
let changeArrayTwo = (prompt('Enter the type of data (format: string, number, true, false, undefined, null)')).toLowerCase();

filterBy(changeArrayOne,changeArrayTwo);