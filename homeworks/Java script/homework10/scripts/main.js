
const firstField = document.querySelector('#first_field');
const secondField = document.querySelector('#second_field');

const firstEye = document.querySelector('.fa-eye');
const secondEye = document.querySelector('.fa-eye-slash');

secondEye.classList.replace("fa-eye-slash", "fa-eye");

const buttonCheck = document.querySelector('#btn_check');

const warningMsg = document.createElement('span');
warningMsg.classList.add('warn_msg');
warningMsg.innerHTML = 'You have not entered a value in some field';
warningMsg.style.color = 'red';
warningMsg.style.fontSize = '14pt';
document.body.append(warningMsg);
warningMsg.hidden = true;

const errorMsg = document.createElement('span');
errorMsg.classList.add('err_msg');
errorMsg.innerHTML = 'You have entered different values. Try again, man!';
errorMsg.style.color = 'red';
errorMsg.style.fontSize = '14pt';
document.body.append(errorMsg);
errorMsg.hidden = true;

firstEye.addEventListener('click', function () {

    if (firstEye.classList.contains("fa-eye")) {
        firstField.type = 'text';
        firstEye.classList.replace("fa-eye", "fa-eye-slash");
    } else if (firstEye.classList.contains("fa-eye-slash")) {
        firstField.type = 'password';
        firstEye.classList.replace("fa-eye-slash","fa-eye");
    }

});

secondEye.addEventListener('click', function () {

    if (secondEye.classList.contains("fa-eye")) {
        secondField.type = 'text';
        secondEye.classList.replace("fa-eye", "fa-eye-slash");
    } else if (secondEye.classList.contains("fa-eye-slash")) {
        secondField.type = 'password';
        secondEye.classList.replace("fa-eye-slash","fa-eye");
    }

});

function checkValues() {
    let firstValue = firstField.value;
    let secondValue = secondField.value;

    if (firstValue != "" && secondValue != "") {
        if (firstValue == secondValue) {
            warningMsg.hidden = true;
            errorMsg.hidden = true;
            alert('You are welcome!');
        } else {
            errorFunc();
        }
    } else {
        emptyFunc();
    }

}

function emptyFunc() {
    warningMsg.hidden = false;
    errorMsg.hidden = true;
}

function errorFunc() {
    errorMsg.hidden = false;
    warningMsg.hidden = true;
}


buttonCheck.addEventListener('click', function () {
    checkValues();
    firstField.value = "";
    secondField.value = "";
});